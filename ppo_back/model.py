import numpy as np
import torch
import torch.nn as nn
from torch.distributions import MultivariateNormal
from ppo_back.distributions import DiagGaussian
from ppo_back.utils import init

class RoundNoGradient(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x):
        return (x * 10**2).round() / (10**2)
    @staticmethod
    def backward(ctx, g):
        return g

class Flatten(nn.Module):
    def forward(self, x):
        return x.view(x.size(0), -1)


class Policy(nn.Module):
    def __init__(self, new_obs_shape, action_space, base=None, base_kwargs=None):
        super(Policy, self).__init__()
        if base_kwargs is None:
            base_kwargs = {}
        if base is None:
            base = MLPBase

        self.base = base(new_obs_shape, **base_kwargs)
        num_outputs = action_space.shape[0]
        self.dist = DiagGaussian(self.base.output_size, num_outputs)

    @property
    def is_recurrent(self):
        return self.base.is_recurrent

    def forward(self, inputs, rnn_hxs, masks):
        raise NotImplementedError

    def act(self, inputs, rnn_hxs, masks, ):
        value, actor_features, rnn_hxs = self.base(inputs, rnn_hxs, masks)
        dist = self.dist(actor_features)

        action = dist.sample()
        action_log_probs = dist.log_probs(action)
        dist_entropy = dist.entropy().mean()
        action = torch.clamp(action, -1, 1)
        return value, action, action_log_probs, rnn_hxs

    def get_value(self, inputs, rnn_hxs, masks):
        value, _, _ = self.base(inputs, rnn_hxs, masks)
        return value

    def evaluate_actions(self, rnn_hxs, masks, actions,  states):
        value, actor_features, rnn_hxs = self.base(states, rnn_hxs, masks)
        dist = self.dist(actor_features)

        action_log_probs = dist.log_probs(actions)
        dist_entropy = dist.entropy().mean()

        return value, action_log_probs, dist_entropy, rnn_hxs

class NNBase(nn.Module):
    def __init__(self, recurrent, recurrent_input_size, hidden_size):
        super(NNBase, self).__init__()

        self._hidden_size = hidden_size
        self._recurrent = recurrent

    @property
    def is_recurrent(self):
        return self._recurrent

    @property
    def output_size(self):
        return self._hidden_size

class MLPBase(NNBase):
    def __init__(self, num_inputs, recurrent=False, hidden_size=64):
        super(MLPBase, self).__init__(recurrent, num_inputs, hidden_size)

        if recurrent:
            num_inputs = hidden_size

        init_ = lambda m: init(m, nn.init.orthogonal_, lambda x: nn.init.
                               constant_(x, 0), np.sqrt(2))

        self.actor = nn.Sequential(
            init_(nn.Linear(num_inputs, hidden_size)), nn.Tanh(),
            init_(nn.Linear(hidden_size, hidden_size)), nn.Tanh())

        self.critic = nn.Sequential(
            init_(nn.Linear(num_inputs, hidden_size)), nn.Tanh(),
            init_(nn.Linear(hidden_size, hidden_size)), nn.Tanh())

        self.critic_linear = init_(nn.Linear(hidden_size, 1))

        self.train()

    def forward(self, inputs, rnn_hxs, masks):
        x = inputs

        hidden_critic = self.critic(x)
        hidden_actor = self.actor(x)

        return self.critic_linear(hidden_critic), hidden_actor, rnn_hxs

class Adversary(nn.Module):
    def __init__(self, obs_shape, action_space, hiddensize = None,
                 min = None, max = None):
        super(Adversary, self).__init__()

        if hiddensize == None:
            hidden_size = 64
        else:
            hidden_size = hiddensize

        if max == None:
            self.max = 2
        else:
            self.max = max

        if min == None:
            self.min = 0.5
        else:
            self.min = min

        init_ = lambda m: init(m, nn.init.orthogonal_, lambda x: nn.init.
                               constant_(x, 0), np.sqrt(2))

        self.adversary = nn.Sequential(
            init_(nn.Linear(obs_shape[0], hidden_size)),
            nn.ReLU(),
            init_(nn.Linear(hidden_size, action_space[0] * 2)),
            nn.Sigmoid())

        self.env_num = obs_shape[0]

    def forward(self, inputs):
        raise NotImplementedError

    def update_limits(self, min, max):
        self.min = min
        self.max = max

    def act(self, inputs):
        output = self.adversary(inputs)
        action_means, action_std = torch.split(output, self.env_num)
        action_mean = action_means * (self.max - self.min) + self.min

        dist = MultivariateNormal(action_mean, torch.diag_embed(action_std*action_std))
        action = dist.sample()
        log_prob = dist.log_prob(action)
        action = torch.clamp(action, self.min, self.max)

        return action, log_prob

    def evaluate(self, input, action):
        # Sampling a distribution is not used when evaluating as it prevents the gradients from flowing.
        output = self.adversary(input)
        action_means, action_std = torch.split(output, self.env_num, 1)
        action_mean = action_means * (self.max - self.min) + self.min

        dist = MultivariateNormal(action_mean, torch.diag_embed(action_std*action_std+0.00001))
        entropy = dist.entropy()
        log_prob = dist.log_prob(action).unsqueeze(-1)

        return entropy.mean(), log_prob
