import numpy as np
import torch

class active_delivery:
    def __init__(self, min, max, interval, env):
        self.max = max
        self.min = min
        self.interval = interval
        self.env = env
        quant = (self.max - self.min)/ self.interval
        self.lengths = np.around(np.linspace(min, max, int(quant)+1),2)
        self.bank = []
        self.count = 0
        self.all_used = []

    def evaluate(self, actor_critic):
        REWARDS = []
        self.count += 1
        for i in self.lengths:
            self.env.update_scale(i)
            obs = self.env.reset()
            done = False
            t = 0
            rewards__ = 0
            recurrent_hidden_states = torch.zeros(1,1)
            masks = torch.zeros(1, 1)
            while not done:
                new_obs = np.concatenate((obs, np.array([i])))
                new_obs = torch.FloatTensor([new_obs])
                value, action, _, recurrent_hidden_states = actor_critic.act(new_obs, recurrent_hidden_states, masks)
                # Obser reward and next obs
                obs, reward, done, info = self.env.step(action.numpy()[0])
                rewards__ += reward
                masks.fill_(0.0 if done else 1.0)
            REWARDS.append(torch.FloatTensor([rewards__]))

        x = 1/torch.stack(REWARDS).numpy().flatten()
        xx = torch.softmax(torch.FloatTensor(x), -1)
        self.norm_rewards = xx.numpy()
        self.bank.append(np.vstack(REWARDS))

    def sample(self):
        scale = np.random.choice(self.lengths, p=self.norm_rewards)
        self.all_used.append(scale)
        return scale

    def check(self):
        return self.bank, self.all_used, self.count
