import torch
from torch.utils.data.sampler import BatchSampler, SubsetRandomSampler


def _flatten_helper(T, N, _tensor):
    return _tensor.view(T * N, *_tensor.size()[2:])
class AdversaryRolloutStorage(object):
    def __init__(self, num_steps, num_processes, new_obs_shape, parameter_space):
        self.obs = torch.zeros(num_steps + 1, num_processes, new_obs_shape*parameter_space)
        self.ratio = torch.zeros(num_steps + 1, num_processes, 1)
        self.action_log_probs = torch.zeros(num_steps, num_processes, 1)
        self.actions = torch.zeros(num_steps, num_processes, new_obs_shape*parameter_space)
        self.num_steps = num_steps
        self.step = 0

    def insert(self, obs, actions, action_log_probs, ratio):
        self.obs[self.step + 1].copy_(obs)
        self.actions[self.step].copy_(actions)
        self.action_log_probs[self.step].copy_(action_log_probs)
        self.ratio[self.step].copy_(ratio)
        self.step = (self.step + 1) % self.num_steps


    def after_update(self):
        self.obs[0].copy_(self.obs[-1])

    def feed_forward_generator(self,
                               ratios,
                               num_mini_batch=None,
                               mini_batch_size=None):
        num_steps, num_processes = self.ratio.size()[0:2]
        batch_size = num_processes * num_steps

        if mini_batch_size is None:
            assert batch_size >= num_mini_batch, (
                "PPO requires the number of processes ({}) "
                "* number of steps ({}) = {} "
                "to be greater than or equal to the number of PPO mini batches ({})."
                "".format(num_processes, num_steps, num_processes * num_steps,
                          num_mini_batch))
            mini_batch_size = batch_size // num_mini_batch
        sampler = BatchSampler(
            SubsetRandomSampler(range(batch_size-1)),
            mini_batch_size,
            drop_last=True)
        for indices in sampler:
            obs_batch = self.obs[:-1].view(-1, *self.obs.size()[2:])[indices]
            actions_batch = self.actions.view(-1,
                                              self.actions.size(-1))[indices]
            old_action_log_probs_batch = self.action_log_probs.view(-1,
                                                                    1)[indices]

            if ratios is None:
                rat = None
            else:
                rat = ratios.view(-1, 1)[indices]


            yield obs_batch, actions_batch, old_action_log_probs_batch, rat
