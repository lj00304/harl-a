from environments.BipedalWalker import *

envs = BipedalWalker()
scale = 1.5
envs.update_scale(scale)
envs.reset()
done = False
while not done:
    # Obser reward and next obs
    obs, reward, done, _ = envs.step(envs.action_space.sample())
    envs.render()