import matplotlib.pyplot as plt
import numpy as np
import torch
from ppo_back.bipedal_walker_friction import *
from stable_baselines import PPO1
import random

envs = BipedalWalker_friction()
print('plotting')
l = np.linspace(0.5, 2.5, 20)
test = 2500
expert_net = PPO1.load('/Volumes/LJ/University/PhD/Year 2/Results/ICRA - first draft/BW/expert networks/ppo/expert_scale_1.zip')
actor_critic, _ = torch.load('/Volumes/LJ/University/PhD/Year 2/Results/ICRA - first draft/BW/friction/friction_BipedalWalker/run_2/BipedalWalker.pt')
recurrent_hidden_states = torch.zeros(1, 1)
masks = torch.zeros(1, 1)
list = np.zeros([10, len(l)])
scale = 0.5
for i in range (10):
    index = 0
    for _ in l:
        scale = _
        envs.update_scale(scale)
        obs = envs.reset()
        done = False
        total_reward = 0
        for steps in range(test):
            int_action = expert_net.predict(obs)
            new_obs = np.concatenate((int_action[0].flatten(), obs, np.array([scale])))
            new_obs = torch.FloatTensor([new_obs])
            value, action, _, recurrent_hidden_states = actor_critic.act(new_obs, recurrent_hidden_states, masks)

            # Obser reward and next obs
            obs, reward, done, _ = envs.step(action[0])
            total_reward += reward
            masks.fill_(0.0 if done else 1.0)
            # envs.render()

            if done:  # steps % 20 == 0 or
                list[i][index] = total_reward + 40
                index += 1
                break
all = list

median = []
maximum = []
minimum = []
for i in range(len(l)):
    med = np.average(all[:, i])
    max = np.max(all[:, i])
    min = np.min(all[:, i])
    median.append(med)
    maximum.append(max)
    minimum.append(min)

envs = BipedalWalker_friction()
print('plotting')
l = np.linspace(0.5, 2.5, 20)
test = 2500
expert_net = PPO1.load('/Volumes/LJ/University/PhD/Year 2/Results/ICRA - first draft/BW/expert networks/ppo/expert_scale_1.zip')
# actor_critic, _ = torch.load('/Users/lucyjackson/PyCharm/harl/HARL/scale/run_1/BW.pt')
recurrent_hidden_states = torch.zeros(1, 1)
masks = torch.zeros(1, 1)
list = np.zeros([10, len(l)])
scale = 0.5
for i in range (10):
    index = 0
    for _ in l:
        scale = _
        envs.update_scale(scale)
        obs = envs.reset()
        done = False
        total_reward = 0
        for steps in range(test):
            int_action = expert_net.predict(obs)
            # new_obs = np.concatenate((int_action[0].flatten(), obs, np.array([scale])))
            # new_obs = torch.FloatTensor([new_obs])
            # value, action, _, recurrent_hidden_states = actor_critic.act(new_obs, recurrent_hidden_states, masks)

            # Obser reward and next obs
            obs, reward, done, _ = envs.step(int_action[0])
            total_reward += reward
            masks.fill_(0.0 if done else 1.0)
            # envs.render()

            if done:  # steps % 20 == 0 or
                list[i][index] = total_reward
                index += 1
                break
all = list

median1 = []
maximum1 = []
minimum1 = []
for i in range(len(l)):
    med1 = np.average(all[:, i])
    max1 = np.max(all[:, i])
    min1 = np.min(all[:, i])
    median1.append(med1)
    maximum1.append(max1)
    minimum1.append(min1)

envs = BipedalWalker_friction()
print('plotting')
l = np.linspace(0.5, 2.5, 20)
test = 2500
# expert_net = PPO1.load('/Volumes/LJ/University/PhD/Year 2/Results/ICRA - first draft/BW/expert networks/ppo/expert_scale_1.zip')
actor_critic, _ = torch.load('/Users/lucyjackson/PyCharm/harl/HCP/friction_only/run_1/BW.pt')
recurrent_hidden_states = torch.zeros(1, 1)
masks = torch.zeros(1, 1)
list = np.zeros([10, len(l)])
scale = 0.5
for i in range (10):
    index = 0
    for _ in l:
        scale = _
        envs.update_scale(scale)
        obs = envs.reset()
        done = False
        total_reward = 0
        for steps in range(test):
            # int_action = expert_net.predict(obs)
            new_obs = np.concatenate((obs, np.array([scale])))
            new_obs = torch.FloatTensor([new_obs])
            value, action, _, recurrent_hidden_states = actor_critic.act(new_obs, recurrent_hidden_states, masks)

            # Obser reward and next obs
            obs, reward, done, _ = envs.step(action[0])
            total_reward += reward
            masks.fill_(0.0 if done else 1.0)
            # envs.render()

            if done:  # steps % 20 == 0 or
                list[i][index] = total_reward - 40
                index += 1
                break
all = list

median2 = []
maximum2 = []
minimum2= []
for i in range(len(l)):
    med2 = np.average(all[:, i])
    max2 = np.max(all[:, i])
    min2 = np.min(all[:, i])
    median2.append(med2)
    maximum2.append(max2)
    minimum2.append(min2)

fig, ax1 = plt.subplots()

ax1.fill_between(l, maximum, minimum,
                 facecolor="blue",  # The fill color
                 # color='blue',       # The outline color
                 alpha=0.2)
ax1.plot(l, median, 'b', label='HARL')

ax1.fill_between(l, maximum1, minimum1,
                 facecolor="green",  # The fill color
                 # color='blue',       # The outline color
                 alpha=0.2)
ax1.plot(l, median1, 'g', label='PPO')

ax1.fill_between(l, maximum2, minimum2,
                 facecolor="red",  # The fill color
                 # color='blue',       # The outline color
                 alpha=0.2)
ax1.plot(l, median2, 'r', label='HCP')

ax1.legend()
ax1.axvline(x=0.75, color ='k', linestyle='--')
ax1.axvline(x=1.25, color ='k', linestyle='--')
ax1.set_ylim(-200,300)
ax1.set_xlabel('Friction')
ax1.set_ylabel('Reward per Episode')
plt.show()