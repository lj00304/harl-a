class Memory:
    # Memory stores each transition in order
    def __init__(self):
        self.actions = []
        self.states = []
        self.logprobs = []
        self.rewards = []
        self.is_terminals = []
        self.cov_mat = []

    def clear_memory(self):
    # Clears the entire memory
        del self.actions[:]
        del self.states[:]
        del self.logprobs[:]
        del self.rewards[:]
        del self.is_terminals[:]
        del self.cov_mat[:]