# HARL-A

All code for HARL-A paper presented at IROS 2021 [1]. The aim of this work is to produce hardware agnostic reinforcment learning agents. This code works with the bipedal walker in open AI gym and a novel environment that replicates a space robot capturing a payload on orbit, code can be found at (https://gitlab.eps.surrey.ac.uk/lj00304/spacerobot_v1).

NOTE: training algorithm can be used with any environment that has the capability to change the physical parameters of the robotic agent with in it. This should be done using function `envs.update_scale(new_physcial_parameter_vector)`.

# Installation
You can install libraries using `pip install -r requirements.txt` note that this work does not use mujoco envs. We suggest using a virtualenv in order to prevent conflicts. 

# Usage
Training is done using `python main_script.py`. There are a number of input parameters that can be set as desired, information on these can be found using the command line `python main_script.py -h`. 

There exists 4 variations of training algorithm in this work. The first two are a reimplementation of hardware conditioned policies [2], where one is as the paper gives (HCP) and the second uses our adversarial sampling technique (HCP-A). The second two are both our novel algorithm where the first uses random sampling (HARL) and the second uses the adversarial sampling technique (HARL-A). The algorithm to be used is selected as follows:

| Algorithm | Sampling technique | Input |
| ------ | ------ | ----- | 
| HPC | random | `main_script.py -E HCP --adversary False` |
| HCP-A | adversarial | `main_script.py -E HCP --adversary True` |
| HARL | random |`main_script.py -E HARL --adversary False`  |
| HARL-A | adversarial | `main_script.py -E HARL --adversary True` |

Note that the space robot operates only with 4 dof and all link lengths are varied regardless of how many dimensions are specified, this is in-line with paper [1].

# Additional information
Videos of results can be found at https://www.youtube.com/watch?v=oxuByrlig2M

## References
- [1] Lucy Jackson, Steve Eckersley, Pete Senior and Simon Hadfield, "HARL-A: Hardware Agnotic Reinforcment Learning Agents Through Adversarial Selection", IROS 2021 (27th of Sept. - 1st Oct.). 
- [2] Tao Chen, Adithyavairavan Murali and Abhinav Gupta, "Hardware conditioned policies for multi-robot transfer learning", Proceedings of the 32nd International Conference on Neural Information Processing Systems, 2018
