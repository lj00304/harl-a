import torch
import torch.nn as nn
from ActorCritic import ActorCritic, Adversary
from ppo_back.Memory import *
from torch.distributions import MultivariateNormal
import gym
import numpy as np
from torch.utils.tensorboard import SummaryWriter
from datetime import datetime
import random
import csv
import copy

class PPO_hardware:
    def __init__(self, env, reward_scheme='SPARSE', length='OFF', K_epochs=80, env_num =5,
                 eps_clip=0.2, gamma=0.9, lr=0.0003, betas=(0.9, 0.999), ref_frame='world', adversary=False):
        # Definition and assigning of hyperparameters
        self.lr = lr
        self.betas = betas
        self.gamma = gamma
        self.env = env
        self.env.reward_scheme = reward_scheme
        self.env.length = length
        self.env.ref_frame = ref_frame
        self.eps_clip = eps_clip
        self.K_epochs = K_epochs
        self.device = torch.device("cpu")

        state_dim = self.env.observation_space.shape[0] + 5
        action_dim = self.env.action_space.shape[0]

        # Networks are initialised. Old network is initalised with the state dict of the first network
        self.policy = ActorCritic(state_dim, action_dim).to(self.device)

        if adversary == True:
            self.adversary = adversary
            self.adversary_policy = Adversary(env_num*4, env_num*4)
            self.env_num = env_num
            self.adversary_optim = torch.optim.Adam(self.adversary_policy.parameters(), lr=lr, betas=betas)
        else:
            self.adversary = None
            self.env_num = env_num

        self.optimizer = torch.optim.Adam(self.policy.parameters(), lr=lr, betas=betas)


        self.policy_old = ActorCritic(state_dim, action_dim).to(self.device)
        self.policy_old.load_state_dict(self.policy.state_dict())

        self.MseLoss = nn.MSELoss()

    def select_action(self, state, memory):
        # action is selected from the old network. This aims to minimise variance.
        state = torch.FloatTensor(state.reshape(1, -1)).to(self.device)
        return self.policy_old.act(state, memory).cpu().data.numpy().flatten()

    def select_action_old(self, state, memory, policy):
        # action is selected from the old network. This aims to minimise variance.
        state = torch.FloatTensor(state.reshape(1, -1)).to(self.device)
        return policy.act(state, memory).cpu().data.numpy().flatten()

    def select_lengths(self, state):
        return self.adversary_policy.act(state)

    def update_adversary(self, memory):
        # Monte Carlo estimate of rewards is done first. This includes the HER_episodes.
        # The reward is discounted at each step and summed to the last step of the episode.
        # This is calculated from complete trajectories.
        rewards = []
        discounted_reward = 0
        for reward in (memory.rewards):
            rewards.append(reward)

        # Normalizing the rewards:
        rewards = torch.FloatTensor(rewards).to(self.device)
        rewards = (rewards - rewards.mean()) / (rewards.std() + 1e-5)

        # convert list  of memory to tensor
        old_states = torch.squeeze(torch.stack(memory.states).to(self.device), 1).detach()
        old_actions = torch.squeeze(torch.stack(memory.actions).to(self.device), 1).detach()
        old_logprobs = torch.squeeze(torch.stack(memory.logprobs), -1).to(self.device).detach()

        # Optimize policy for K epochs:
        for _ in range(self.K_epochs):
            # Evaluating old actions and states.
            dist_entropy, logprobs = self.adversary_policy.evaluate(old_states, old_actions)
            if logprobs[0] == 0:
                loss = np.array([0,0,0])
                break
            # Finding the ratio (pi_theta / pi_theta__old), where pi represents the probability of that action being
            # chosen from that state. This gives the ratio between the old policy and the current policy
            ratios = torch.exp(logprobs - old_logprobs.detach())

            # The advantage function is calculated by taking the rewards away from the baseline estimate of the value of
            # any state (from critic network)
            advantages = rewards

            # surr1 is the unclipped objective function - this is a standard policy gradient update
            # surr2 is the clipped objective function - this prevents updates from occuring far from the current policy
            surr1 = ratios * advantages
            surr2 = torch.clamp(ratios, 1 - self.eps_clip, 1 + self.eps_clip) * advantages

            # final loss from PPO paper 0.5 and 0.01 are coefficients.
            # loss for network sums the min of the two objective functions, the loss for the critic network and an entropy
            # term to force exploration.
            loss = -torch.min(surr1, surr2) - 0.001 * dist_entropy

            # take gradient step
            self.adversary_optim.zero_grad()
            loss.mean().backward()
            self.adversary_optim.step()

        return loss.mean()

    def old_update(self, memory, policy, opt):
        # Monte Carlo estimate of rewards is done first. This includes the HER_episodes.
        # The reward is discounted at each step and summed to the last step of the episode.
        # This is calculated from complete trajectories.
        rewards = []
        discounted_reward = 0
        for reward, is_terminal in zip(reversed(memory.rewards), reversed(memory.is_terminals)):
            if is_terminal:
                discounted_reward = 0
            discounted_reward = reward + (self.gamma * discounted_reward)
            rewards.insert(0, discounted_reward)

        # Normalizing the rewards:
        rewards = torch.FloatTensor(rewards).to(self.device)
        rewards = (rewards - rewards.mean()) / (rewards.std() + 1e-5)

        # convert list  of memory to tensor
        old_states = torch.squeeze(torch.stack(memory.states).to(self.device)).detach()
        old_actions = torch.squeeze(torch.stack(memory.actions).to(self.device)).detach()
        old_logprobs = torch.squeeze(torch.stack(memory.logprobs), -1).to(self.device).detach()
        old_cov_mat = torch.squeeze(torch.stack(memory.cov_mat), -1).to(self.device).detach()

        # Optimize policy for K epochs:
        for _ in range(self.K_epochs):
            # Evaluating old actions and states.
            logprobs, state_values, dist_entropy = policy.evaluate(old_states, old_actions)
            if logprobs[0] == 0:
                loss = np.array([0,0,0])
                break
            # Finding the ratio (pi_theta / pi_theta__old), where pi represents the probability of that action being
            # chosen from that state. This gives the ratio between the old policy and the current policy
            ratios = torch.exp(logprobs - old_logprobs.detach())

            # The advantage function is calculated by taking the rewards away from the baseline estimate of the value of
            # any state (from critic network)
            advantages = rewards - state_values.detach()

            # surr1 is the unclipped objective function - this is a standard policy gradient update
            # surr2 is the clipped objective function - this prevents updates from occuring far from the current policy
            surr1 = ratios * advantages
            surr2 = torch.clamp(ratios, 1 - self.eps_clip, 1 + self.eps_clip) * advantages

            # final loss from PPO paper 0.5 and 0.01 are coefficients.
            # loss for network sums the min of the two objective functions, the loss for the critic network and an entropy
            # term to force exploration.
            loss = -torch.min(surr1, surr2) + 0.5 * self.MseLoss(state_values, rewards) - 0.01 * dist_entropy

            # take gradient step
            opt.zero_grad()
            loss.mean().backward()
            opt.step()

        return loss.mean()

    def update(self, memory):
        # Monte Carlo estimate of rewards is done first. This includes the HER_episodes.
        # The reward is discounted at each step and summed to the last step of the episode.
        # This is calculated from complete trajectories.
        rewards = []
        discounted_reward = 0
        for reward, is_terminal in zip(reversed(memory.rewards), reversed(memory.is_terminals)):
            if is_terminal:
                discounted_reward = 0
            discounted_reward = reward + (self.gamma * discounted_reward)
            rewards.insert(0, discounted_reward)

        # Normalizing the rewards:
        rewards = torch.FloatTensor(rewards).to(self.device)
        rewards = (rewards - rewards.mean()) / (rewards.std() + 1e-5)

        # convert list  of memory to tensor
        old_states = torch.squeeze(torch.stack(memory.states).to(self.device)).detach()
        old_actions = torch.squeeze(torch.stack(memory.actions).to(self.device)).detach()
        old_logprobs = torch.squeeze(torch.stack(memory.logprobs), -1).to(self.device).detach()
        old_cov_mat = torch.squeeze(torch.stack(memory.cov_mat), -1).to(self.device).detach()

        # Optimize policy for K epochs:
        for _ in range(self.K_epochs):
            # Evaluating old actions and states.
            logprobs, state_values, dist_entropy = self.policy.evaluate(old_states, old_actions)
            if logprobs[0] == 0:
                loss = np.array([0,0,0])
                break
            # Finding the ratio (pi_theta / pi_theta__old), where pi represents the probability of that action being
            # chosen from that state. This gives the ratio between the old policy and the current policy
            ratios = torch.exp(logprobs - old_logprobs.detach())

            # The advantage function is calculated by taking the rewards away from the baseline estimate of the value of
            # any state (from critic network)
            advantages = rewards - state_values.detach()

            # surr1 is the unclipped objective function - this is a standard policy gradient update
            # surr2 is the clipped objective function - this prevents updates from occuring far from the current policy
            surr1 = ratios * advantages
            surr2 = torch.clamp(ratios, 1 - self.eps_clip, 1 + self.eps_clip) * advantages

            # final loss from PPO paper 0.5 and 0.01 are coefficients.
            # loss for network sums the min of the two objective functions, the loss for the critic network and an entropy
            # term to force exploration.
            loss = -torch.min(surr1, surr2) + 0.5 * self.MseLoss(state_values, rewards) - 0.01 * dist_entropy

            # take gradient step
            self.optimizer.zero_grad()
            loss.mean().backward()
            self.optimizer.step()

        if logprobs[0] != 0:
            # Copy new weights into old policy:
            self.policy_old.load_state_dict(self.policy.state_dict())

        return loss.mean()

    def HER(self, memory, HER_memory, final_error, index, env, count):
        # need to trim the current memory based on the index to get one episode replay.
        # the final_error is taken from the env
        # the index corresponds to the position in the memory that the most recent episode starts at.

        # Checks that there is a value for the final error, if not, the HER episode is skipped
        f = np.any(np.isnan(final_error)) \
            or np.any(np.isinf(final_error))

        if f:
            count += 0
        else:
            # states and rewards for this episode are extracted from the memory
            o_states = memory.states[index::]
            o_rewards = memory.rewards[index::]

            n_states = []
            n_rewards = []
            n_is_terminals = []

            # The error is worked out based on the size of the payload and the final error.
            # It is calculated as a scalar factor by which to multiply the final error between the base
            # and the payload by. This gives the final vector error that can be subtracted from the state.
            size = env.payload.get_size()
            ratio = (np.linalg.norm(final_error) - size[0]) / np.linalg.norm(final_error)
            err = final_error * ratio

            # error vector is set up that moves the payload by the error between the base and payload
            # for the final state in the episode
            error = torch.zeros(22+(self.env.dof*2))
            error[12+(self.env.dof*2)] = err[0]
            error[13+(self.env.dof*2)] = err[1]
            error[14+(self.env.dof*2)] = err[2]

            # All the states in the episode are itterated over, subtracting the error from it.
            # The new reward is also calculated based on the state.
            # The new states, rewards and is_terminals are stored.
            for i in range(len(o_states)):
                zeros = torch.FloatTensor([0, 0, 0, 0, 0])
                new_error = torch.cat([error,zeros])
                n_state = o_states[i]-new_error
                n_states.append(n_state)
                state = n_state.numpy()
                n_reward, n_terminal, captured = env.HER_reward(state[0])
                n_rewards.append(n_reward)
                n_is_terminals.append(n_terminal)

                # When the episode has terminated the corresponding actions, covariance and logprobs
                """
                check if the log_probs need to be re-calcuated for the new state.   
                """
                if n_terminal == True:
                    if n_reward > 39:
                        count += 1
                    n_actions = memory.actions[index:(index + i + 1)]
                    n_logprobs = memory.logprobs[index:(index + i + 1)]
                    n_cov_mat = memory.cov_mat[index:(index + i + 1)]
                    # n_is_terminals = memory.is_terminals[index:(index + i + 1)]
                    # n_is_terminals.append(n_terminal)

                    # All variables are stored in the HER_memory
                    HER_memory.rewards.extend(n_rewards)
                    HER_memory.states.extend([n_states])
                    HER_memory.actions.extend(n_actions)
                    HER_memory.logprobs.extend(
                        n_logprobs)  # remain the same as these stem from the action not the state
                    HER_memory.cov_mat.extend(
                        n_cov_mat)  # remain the same as these stem from the action not the state
                    HER_memory.is_terminals.extend(n_is_terminals)

                    # The episode is first removed from the memory
                    # This is not actually necessary, could just add the HER memory to the buffer. This might speed up
                    # the process as you would collect more data, the HER frequency would need to be increased though
                    memory.rewards = memory.rewards[:index]
                    memory.states = memory.states[:index]
                    memory.actions = memory.actions[:index]
                    memory.logprobs = memory.logprobs[:index]
                    memory.cov_mat = memory.cov_mat[:index]
                    memory.is_terminals = memory.is_terminals[:index]

                    # HER episode is appended to the memory.
                    memory.rewards.extend(n_rewards)
                    memory.states.extend(n_states)
                    memory.actions.extend(n_actions)
                    memory.logprobs.extend(n_logprobs)
                    memory.cov_mat.extend(n_cov_mat)
                    memory.is_terminals.extend(n_is_terminals)
                    break
        return count

    def train(self, env_num=5, HER_frequency = 2, log_interval=50, max_episodes=40000, max_timesteps=100, update_timestep=2000, minimum=0.1, maximum=0.5, her_on=True, write=True, render=False):

        now_time = datetime.now().strftime('%Y%m%d-%H%M')
        if write == True:
            if self.adversary:
                writer = SummaryWriter(
                f"./logging/HCP-a/SR/{now_time}/tensor_board")
                f = open(f"./logging/HCP-a/SR/{now_time}/log.csv", "wt")
            else:
                writer = SummaryWriter(
                f"./logging/HCP/SR/{now_time}/tensor_board")
                f = open(f"./logging/HCP/SR/{now_time}/log.csv", "wt")

            logger = csv.DictWriter(f, fieldnames=('r', 'cap', 'l1', 'l2', 'l3', 'l4'))
            if self.adversary == True:
                self.adversary_policy.update_limits(minimum, maximum)
                logger = csv.DictWriter(f, fieldnames=('r', 'cap', 'l1', 'l2', 'l3', 'l4'))
            logger.writeheader()

        if render:
            self.env.initialise_render()

        # Definition of training conditions.
        memory = Memory()
        HER_memory = Memory()
        ad_memory = Memory()

        # logging variables
        running_reward = 0
        avg_length = 0
        time_step = 0
        captured = 0
        her = 0
        count = 0
        parameter_space = 4

        agent_episodes = 15000
        adversary_episodes = 1000
        shorter_max_timesteps = 300
        adversary_update = 32
        shorter_update_timestep = 50

        num_updates =int(max_episodes // agent_episodes)

        if self.adversary == True:
            adversary_state = torch.rand(self.env_num * 4)
            ad_mask = torch.zeros(env_num)
            ad_mask[random.randint(0, env_num - 1)] = 1
            scales, log_prob = self.select_lengths(adversary_state)
            scales = torch.clamp(scales, minimum, maximum)
            scale = torch.mm(ad_mask.unsqueeze(-1).T, scales.reshape(env_num, parameter_space)).detach().numpy()

        else:
            l1 = random.uniform(minimum, maximum)
            l2 = random.uniform(minimum, maximum)
            l3 = random.uniform(minimum, maximum)
            l4 = random.uniform(minimum, maximum)
            scale = (np.array([[l1, l2, l3, l4]]))

        # training loop
        # this runs for a maximum number of episodes.
        for k in range(num_updates):
            for i_episode in range(1, agent_episodes):
                self.env.update_scale(scale[0])
                int_obs = self.env.reset()
                rewards = 0

                # each episode runs up to a maximum time step
                for t in range(max_timesteps):
                    time_step += 1
                    new_obs = torch.FloatTensor(np.concatenate((int_obs, self.env.get_link_length())))
                    # Running policy_old and using this to select the actions
                    action = self.select_action(new_obs, memory)
                    int_obs, reward, done, x = self.env.step(action)

                    if render:
                        self.env.render()

                    # Saving reward and is_terminals:
                    memory.rewards.append(reward)
                    memory.is_terminals.append(done)

                    avg_length += 1

                    # update if enough time steps have been carried out.
                    # this is done based on the memory length since with the HER algorithm some episodes
                    # end up being shortened due to premature collisions
                    if len(memory.rewards) % update_timestep == 0:
                        loss = self.update(memory)
                        # Memory is then cleared if the replay buffer is not being used.
                        memory.clear_memory()
                        if write == True:
                            writer.add_scalar('Loss', loss, i_episode)
                        print('############## UPDATED ##############')
                        time_step = 0
                    running_reward += reward
                    rewards += reward

                    # if the episode has come to and end the the loop is exited.
                    if done:
                        if self.adversary == True:
                            adversary_state = torch.rand(self.env_num * 4)
                            ad_mask = torch.zeros(env_num)
                            ad_mask[random.randint(0, env_num - 1)] = 1
                            scales, log_prob = self.select_lengths(adversary_state)
                            scales = torch.clamp(scales, minimum, maximum)
                            scale = torch.mm(ad_mask.unsqueeze(-1).T,
                                             scales.reshape(env_num, parameter_space)).detach().numpy()
                            l1, l2, l3, l4 = scale[0]
                            infos = {'r':rewards, 'l1':l1, 'l2':l2, 'l3':l3, 'l4':l4, 'cap':x }
                            if write == True:
                                logger.writerow(infos)
                        else:
                            l1 = random.uniform(minimum, maximum)
                            l2 = random.uniform(minimum, maximum)
                            l3 = random.uniform(minimum, maximum)
                            l4 = random.uniform(minimum, maximum)
                            scale = (np.array([[l1, l2, l3, l4]]))
                            infos = {'r': rewards, 'cap': x, 'l1': l1, 'l2': l2, 'l3': l3, 'l4': l4}
                            if write == True:
                                logger.writerow(infos)
                        break

                # a HER episode is only ever added at the end of a full episode.
                # Their frequency is dependent on a parameter set out initially.
                if her_on == True:
                    if i_episode % HER_frequency == 0:
                        final_error = self.env.final_position_error()
                        index = len(memory.rewards) - (t + 1)
                        count = self.HER(memory, HER_memory, final_error, index, self.env, count)
                        her += 1

                # number of times that the payload is sucessfully captured is logged.
                captured += x

                if write == True:
                    writer.add_scalar('Episode Reward', rewards, i_episode)
                    writer.add_scalar('Episode Time', t, i_episode)
                    rewards = 0

                # logging
                if i_episode % log_interval == 0:
                    avg_length = (avg_length / log_interval)
                    running_reward = int(running_reward / log_interval)

                    if write == True:
                        writer.add_scalar('Times Captured', captured, i_episode)

                    print(
                        'Episode {} \t Avg length: {} \t Avg reward: {} \t Times Captured: {} \t Good HER episodes: {}/{}'.format(
                            i_episode, avg_length,
                            running_reward,
                            captured, count, her))
                    running_reward = 0
                    avg_length = 0
                    captured = 0
                    her = 0
                    count = 0

                if i_episode % 50 == 0:
                    if self.adversary:
                        torch.save([
                            self.policy,
                            getattr(self.env, 'ob_rms', None)
                        ], f"./logging/HCP-a/SR/{now_time}/space_robot.pt")
                    else:
                        torch.save([
                            self.policy,
                            getattr(self.env, 'ob_rms', None)
                        ], f"./logging/HCP/SR/{now_time}/space_robot.pt")


            if self.adversary == True:
                print('ADVERSARY')
                old_policy = copy.deepcopy(self.policy)
                optimizer2 = type(self.optimizer)(old_policy.parameters(), lr=self.optimizer.defaults['lr'])
                optimizer2.load_state_dict(self.optimizer.state_dict())
                list_number = range(0, env_num)
                list_pos = 0

                test_eps = 100

                adversary_state = torch.rand(self.env_num * 4)
                ad_mask = torch.zeros(env_num)
                ad_mask[list_number[list_pos]] = 1
                scales, log_prob = self.select_lengths(adversary_state)
                scales = torch.clamp(scales, minimum, maximum)
                scale = torch.mm(ad_mask.unsqueeze(-1).T, scales.reshape(env_num, parameter_space)).detach().numpy()

                for i_episode in range(1, adversary_episodes):
                    count = 0
                    for K in range(4):
                        self.env.update_scale(scale[0])
                        int_obs = self.env.reset()
                        # each episode runs up to a maximum time step
                        for test_eps in range(test_eps):
                            for t in range(shorter_max_timesteps):
                                time_step += 1
                                new_obs = torch.FloatTensor(np.concatenate((int_obs, self.env.get_link_length())))
                                # Running policy_old and using this to select the actions
                                action = self.select_action_old(new_obs, memory, old_policy)
                                int_obs, reward, done, x = self.env.step(action)

                                if render:
                                    self.env.render()

                                # Saving reward and is_terminals:
                                memory.rewards.append(reward)
                                memory.is_terminals.append(done)

                                avg_length += 1

                                # update if enough time steps have been carried out.
                                # this is done based on the memory length since with the HER algorithm some episodes
                                # end up being shortened due to premature collisions
                                if len(memory.rewards) % shorter_update_timestep == 0:
                                    loss = self.old_update(memory, old_policy, optimizer2)
                                    # Memory is then cleared if the replay buffer is not being used.
                                    memory.clear_memory()
                                    if write == True:
                                        writer.add_scalar('Loss', loss, i_episode)
                                    time_step = 0

                                # if the episode has come to and end the the loop is exited.
                                if done:
                                    if list_pos < env_num - 1:
                                        list_pos += 1
                                    else:
                                        list_pos = 0
                                    ad_mask = torch.zeros(env_num)
                                    ad_mask[list_number[list_pos]] = 1
                                    scale = torch.mm(ad_mask.unsqueeze(-1).T,
                                                     scales.reshape(env_num, parameter_space)).detach().numpy()
                                    self.env.update_scale(scale[0])
                                    int_obs = self.env.reset()
                                    break
                            captured += x
                        if count == 0:
                            first_reward = captured
                        if count == 3:
                            last_reward = captured
                        captured = 0
                        count += 1

                    ad_memory.states.append(adversary_state)
                    ad_memory.actions.append(scales)
                    ad_memory.logprobs.append(log_prob)
                    ad_memory.rewards.append((100+last_reward)/(100+first_reward))
                    # print('ratio is {}'.format((100+last_reward)/(100+first_reward)))
                    if write == True:
                        writer.add_scalar('ratio', (100+last_reward)/(100+first_reward), i_episode)

                    if len(ad_memory.rewards) % adversary_update == 0:
                        loss = self.update_adversary(ad_memory)
                        # Memory is then cleared if the replay buffer is not being used.
                        ad_memory.clear_memory()

                if i_episode % 50 == 0:
                    if self.adversary:
                        torch.save([
                            self.adversary_policy,
                            getattr(self.env, 'ob_rms', None)
                        ], f"./logging/HCP-a/SR/{now_time}/adversary_space_robot.pt")
