import os
from collections import deque
import csv
import torch
from environments.BipedalWalker import *
from environments.BipedalWalker_friction import *
from environments.CartPole import *
from ppo_back import utils
from ppo_back.model import Policy
from ppo_back.storage import RolloutStorage
from ppo_back.ppo import PPO
from environments.BipedalWalker_indi import *
from environments.CartPole_indi import *
from torch.utils.tensorboard import SummaryWriter
from datetime import datetime

class hcp:
    def __init__(self, args):
        self.ad_entropy = 0.001
        self.env_name = args.environment
        self.env_num = 5
        self.lower = args.lower
        self.upper = args.upper
        self.parameter_space = args.dimension

        self.num_env_steps = args.N            # number of steps to train for
        self.num_processes = 1                 # how many cpu processes to use
        self.log_interval = args.log_int                   # log interval
        self.save_interval = args.save_int                # save interval


        self.log_dir = f"./logging/HCP/{self.env_name}_dim_{self.parameter_space}/{datetime.now().strftime('%Y%m%d-%H%M')}" # log directory
        self.save_dir = f"./logging/HCP/{self.env_name}_dim_{self.parameter_space}/{datetime.now().strftime('%Y%m%d-%H%M')}"
        self.writer = SummaryWriter(
            f"./logging/HCP/{self.env_name}_dim_{self.parameter_space}/{datetime.now().strftime('%Y%m%d-%H%M')}/tensor_board")

        # parameters that gouvern the ppo algorithm
        self.clip_param = 0.2        # ppo clip parameter
        self.ppo_epoch = 4           # number of epochs
        self.num_mini_batch = 32     # number of batches
        self.value_loss_coef = 0.5   # value loss coefficient
        self.entropy_coef = 0.01     # entropy term coefficient
        self.gamma = 0.99            # discount factor for rewards
        self.lr = 7e-4               # learning rate
        self.eps = 1e-5              # RMSprop optimizer epsilon
        self.max_grad_norm = 0.5     # max normal of gradients
        self.num_steps = 2500        # number of forward steps allowed in one ep
        self.num_ad_steps = 200     # number of episodes needed for adversary update
        self.gae_lambda = 0.95       # lambda parameter for GAE

        self.log_dir = os.path.expanduser(self.log_dir)
        utils.cleanup_log_dir(self.log_dir)

    def train_single(self):
        f = open('./{}/log.csv'.format(self.log_dir), "wt")
        logger = csv.DictWriter(f, fieldnames=('r', 'l', 't', 'scale'))
        logger.writeheader()
        environments = {'bipedal_walker_friction': BipedalWalker_friction(),
                        'bipedal_walker': BipedalWalker(),
                        'cart_pole': CartPole()}
        try:
            envs = environments[self.env_name]
        except KeyError:
            sys.exit('Unsuitable env')

        actor_critic = Policy(
            envs.observation_space.shape[0]+self.parameter_space,
            envs.action_space,)

        agent = PPO(
            actor_critic,
            self.clip_param,
            self.ppo_epoch,
            self.num_mini_batch,
            self.value_loss_coef,
            self.entropy_coef,
            self.ad_entropy,
            lr=self.lr,
            eps=self.eps,
            max_grad_norm=self.max_grad_norm)

        rollouts = RolloutStorage(self.num_steps, self.num_processes,
                                  envs.observation_space.shape[0]+self.parameter_space,
                                  envs.action_space)

        scale = random.uniform(self.lower, self.upper)
        envs.update_scale(scale)
        int_obs = envs.reset()

        obs = torch.FloatTensor(np.concatenate((int_obs, np.array([scale]))))

        rollouts.obs[0].copy_(obs)

        episode_rewards = deque(maxlen=10)

        start = time.time()
        num_updates = int(
            self.num_env_steps) // self.num_steps // self.num_processes

        t = 0
        tt = 0
        count = 0
        total_reward = 0


        for j in range(num_updates):
            for step in range(self.num_steps):
                # Sample actions
                with torch.no_grad():
                    value, action, action_log_prob, recurrent_hidden_states = actor_critic.act(rollouts.obs[step],
                        rollouts.recurrent_hidden_states[step], rollouts.masks[step])

                # Observe reward and next obs
                int_obs, reward, done, infos = envs.step(action.numpy()[0])

                obs = torch.FloatTensor(np.concatenate((int_obs, np.array([scale]))))
                total_reward += reward

                if 'episode' in infos.keys():
                    self.writer.add_scalar('reward', infos['episode']['r'], t)
                    self.writer.add_scalar('Scale', scale, t)
                    episode_rewards.append(infos['episode']['r'])
                    t += 1

                # If done then clean the history of observations.
                masks = torch.FloatTensor([[0.0] if done else [1.0]])
                bad_masks = torch.FloatTensor([[0.0] if 'bad_transitions' in infos.keys() else [1.0]])

                ### look into if the masks are missmatched or not.
                if done:
                    logger.writerow(infos['episode'])
                    total_reward = 0
                    scale = random.uniform(self.lower, self.upper)
                    envs.update_scale(scale)
                    int_obs = envs.reset()
                    obs = torch.FloatTensor(np.concatenate((int_obs, np.array([scale]))))
                    # only include finished episodes in the adversary buffer.

                rollouts.insert(obs, recurrent_hidden_states, action, action_log_prob,
                   value, torch.FloatTensor([[reward]]), masks, bad_masks)

            with torch.no_grad():
                next_value = actor_critic.get_value(
                    rollouts.obs[-1], rollouts.recurrent_hidden_states[-1],
                    rollouts.masks[-1]).detach()

            rollouts.compute_returns(next_value, self.gamma,
                                     self.gae_lambda)

            value_loss, action_loss, dist_entropy = agent.update_agent(rollouts)
            count += 1
            self.writer.add_scalar('value loss', value_loss, tt)
            self.writer.add_scalar('action loss', action_loss, tt)
            tt += 1

            rollouts.after_update()

                # save for every interval-th episode or for the last epoch
            if (j % self.save_interval == 0
                    or j == num_updates - 1) and self.save_dir != "":
                save_path = os.path.join(self.save_dir)
                try:
                    os.makedirs(save_path)
                except OSError:
                    pass

                torch.save([
                    actor_critic,
                    getattr(envs, 'ob_rms', None)
                ], os.path.join(save_path, self.env_name + ".pt"))

            if j % self.log_interval == 0 and len(episode_rewards) > 1:
                total_num_steps = (j + 1) * self.num_processes * self.num_steps
                end = time.time()
                print(
                    "Updates {}, num timesteps {}, FPS {} \n Last {} training episodes: mean/median reward {:.1f}/{:.1f}, min/max reward {:.1f}/{:.1f}\n"
                    .format(j, total_num_steps,
                            int(total_num_steps / (end - start)),
                            len(episode_rewards), np.mean(episode_rewards),
                            np.median(episode_rewards), np.min(episode_rewards),
                            np.max(episode_rewards)))

    def train_double(self):
        environments = {'cart_pole': CartPole_indi(),
                        'bipedal_walker': BipedalWalker_indi()}

        try:
            envs = environments[self.env_name]
        except KeyError:
            sys.exit('Unsuitable env')

        f = open('./{}/log.csv'.format(self.log_dir), "wt")
        logger = csv.DictWriter(f, fieldnames=('r', 'l', 't', 'scale', 'mass'))
        logger.writeheader()

        if self.env_name == 'bipedal_walker':
            self.parameter_space = 8
            f = open('./{}/log.csv'.format(self.log_dir), "wt")
            logger = csv.DictWriter(f, fieldnames=('r', 'l1', 'l2',
                                                   'l3', 'l4', 'l5',
                                                   'l6', 'l7', 'l8',
                                                   't'))
            logger.writeheader()

        actor_critic = Policy(
            envs.observation_space.shape[0]+self.parameter_space,
            envs.action_space, )

        agent = PPO(
            actor_critic,
            self.clip_param,
            self.ppo_epoch,
            self.num_mini_batch,
            self.value_loss_coef,
            self.entropy_coef,
            self.ad_entropy,
            lr=self.lr,
            eps=self.eps,
            max_grad_norm=self.max_grad_norm)

        rollouts = RolloutStorage(self.num_steps, self.num_processes,
                                  envs.observation_space.shape[0]+self.parameter_space,
                                  envs.action_space)
        list_pos = 0
        scale = []
        for i in range(self.parameter_space):
            scale.append(random.uniform(self.lower, self.upper))

        scale = np.stack(scale)
        envs.update_scale(scale)
        int_obs = envs.reset()

        obs = torch.FloatTensor(np.concatenate((int_obs, scale)))

        rollouts.obs[0].copy_(obs)

        episode_rewards = deque(maxlen=10)

        start = time.time()
        num_updates = int(
            self.num_env_steps) // self.num_steps // self.num_processes

        t = 0
        tt = 0
        count = 0
        total_reward = 0

        first_rewards = []

        for j in range(num_updates):
            for step in range(self.num_steps):
                # Sample actions
                with torch.no_grad():
                    value, action, action_log_prob, recurrent_hidden_states = actor_critic.act(rollouts.obs[step],
                                                                                               rollouts.recurrent_hidden_states[
                                                                                                   step],
                                                                                               rollouts.masks[step])

                # Observe reward and next obs
                int_obs, reward, done, infos = envs.step(action.numpy()[0])

                obs = torch.FloatTensor(np.concatenate((int_obs, scale)))
                total_reward += reward

                if 'episode' in infos.keys():
                    self.writer.add_scalar('reward', infos['episode']['r'], t)
                    episode_rewards.append(infos['episode']['r'])
                    t += 1

                # If done then clean the history of observations.
                masks = torch.FloatTensor([[0.0] if done else [1.0]])
                bad_masks = torch.FloatTensor([[0.0] if 'bad_transitions' in infos.keys() else [1.0]])

                ### look into if the masks are missmatched or not.
                if done:
                    if count == 0 and len(first_rewards) < self.env_num:
                        first_rewards.append([total_reward, scale])
                    if list_pos < self.env_num - 1:
                        list_pos += 1
                    else:
                        list_pos = 0
                    logger.writerow(infos['episode'])
                    scale = []
                    for i in range(self.parameter_space):
                        scale.append(random.uniform(self.lower, self.upper))
                    scale = np.stack(scale)
                    envs.update_scale(scale)
                    int_obs = envs.reset()
                    obs = torch.FloatTensor(np.concatenate((int_obs, scale)))
                    # only include finished episodes in the adversary buffer.

                rollouts.insert(obs, recurrent_hidden_states, action, action_log_prob,
                                value, torch.FloatTensor([[reward]]), masks, bad_masks)

            with torch.no_grad():
                next_value = actor_critic.get_value(
                    rollouts.obs[-1], rollouts.recurrent_hidden_states[-1],
                    rollouts.masks[-1]).detach()

            rollouts.compute_returns(next_value, self.gamma,
                                     self.gae_lambda)

            value_loss, action_loss, dist_entropy = agent.update_agent(rollouts)
            count += 1
            self.writer.add_scalar('value loss', value_loss, tt)
            self.writer.add_scalar('action loss', action_loss, tt)
            tt += 1

            rollouts.after_update()

            # save for every interval-th episode or for the last epoch
            if (j % self.save_interval == 0
                or j == num_updates - 1) and self.save_dir != "":
                save_path = os.path.join(self.save_dir)
                try:
                    os.makedirs(save_path)
                except OSError:
                    pass

                torch.save([
                    actor_critic,
                    getattr(envs, 'ob_rms', None)
                ], os.path.join(save_path, self.env_name + ".pt"))

            if j % self.log_interval == 0 and len(episode_rewards) > 1:
                total_num_steps = (j + 1) * self.num_processes * self.num_steps
                end = time.time()
                print(
                    "Updates {}, num timesteps {}, FPS {} \n Last {} training episodes: mean/median reward {:.1f}/{:.1f}, min/max reward {:.1f}/{:.1f}\n"
                        .format(j, total_num_steps,
                                int(total_num_steps / (end - start)),
                                len(episode_rewards), np.mean(episode_rewards),
                                np.median(episode_rewards), np.min(episode_rewards),
                                np.max(episode_rewards)))
