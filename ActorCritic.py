import torch
import torch.nn as nn
from torch.distributions import MultivariateNormal
import numpy as np
from ppo_back.sr_utils import *

class ActorCritic(nn.Module):
    # actor network generates a mean and standard deviation for each possible action
    def __init__(self, state_dim, action_dim):
        super(ActorCritic, self).__init__()
        # action mean range -1 to 1
        self.actor = nn.Sequential(
            nn.Linear(state_dim, 64),
            nn.Tanh(),
            nn.Linear(64, 32),
            nn.Tanh(),
            nn.Linear(32, action_dim * 2),
            nn.Tanh()
        )
        # critic network is used to evaluate each state during the learning process
        self.critic = nn.Sequential(
            nn.Linear(state_dim, 64),
            nn.Tanh(),
            nn.Linear(64, 32),
            nn.Tanh(),
            nn.Linear(32, 1)
        )

        self.action_dim = action_dim

    def forward(self):
        raise NotImplementedError

    def act(self, state, memory):
        # Act function is called when collecting sample trajectories.
        # The agent acts based on a prediction from the actor network. The first 9 elements correspond to the means of each action
        # the next 9 elements correspond to the covariance of each. The covariance is used since this means that a negative standard
        # deviation will never be reached
        output = self.actor(state)

        # split the network output into means and std
        action_mean, action_std = torch.split(output[0], self.action_dim)

        # calcualte the covariance and distribute it to be the diagonal elements of a square matrix.
        cov_mat = torch.diag(action_std*action_std)

        # determines the multinomial distribution that the actions can be sampled from.
        # except loop ensures that if the network outputs nan for the std training can continue.
        # This 0.02 (forced covariance) could become a hyperparameter.
        try:
            dist = MultivariateNormal(action_mean, cov_mat)
        except:
            cov_mat = torch.zeros(self.action_dim, self.action_dim)
            cov_mat[range(self.action_dim), range(self.action_dim)] = 0.2
            dist = MultivariateNormal(action_mean, cov_mat)

        # samples action and calculates the log_prob of that action
        action = dist.sample()
        action_logprob = dist.log_prob(action)

        # all relevant items are stored in the memory.
        memory.states.append(state)
        memory.actions.append(action)
        memory.logprobs.append(action_logprob)
        memory.cov_mat.append(cov_mat)

        return action.detach()

    def evaluate(self, state, action):
        # Evaluation does a similar thing to the act function but it is called on stored data and outputs the parameters
        # needed for the agent to learn (log_prob of the action, entropy and the state value)
        # The state value is predicted by the second network - which has an output of one. This is used to then calculate
        # the advantage function.

        action_mean, std = torch.split(self.actor(state), self.action_dim, 1)
        cov_mats = torch.diag_embed(std*std)

        try:
            dist = MultivariateNormal(action_mean, cov_mats)
        except:
        # this is producing a runtime error of different sized tensors.
            cov_mat = torch.zeros(self.action_dim, self.action_dim)
            cov_mat[range(self.action_dim), range(self.action_dim)] = 0.2
            dist = MultivariateNormal(action_mean, cov_mat)

        try:
            action_logprobs = dist.log_prob(action)
            dist_entropy = dist.entropy()
        except:
            action_logprobs = np.array([0,0,0])
            dist_entropy = 0

        state_value = self.critic(state)

        return action_logprobs, torch.squeeze(state_value), dist_entropy



class Adversary(nn.Module):
    def __init__(self, obs_shape, action_space, hiddensize = None,
                 min = None, max = None):
        super(Adversary, self).__init__()

        if hiddensize == None:
            hidden_size = 64
        else:
            hidden_size = hiddensize

        if max == None:
            self.max = 0.5
        else:
            self.max = max

        if min == None:
            self.min = 0.1
        else:
            self.min = min

        init_ = lambda m: init(m, nn.init.orthogonal_, lambda x: nn.init.
                               constant_(x, 0), np.sqrt(2))

        self.adversary = nn.Sequential(
            init_(nn.Linear(obs_shape, hidden_size)),
            nn.ReLU(),
            init_(nn.Linear(hidden_size, action_space * 2)),
            nn.Sigmoid())

        self.env_num = int(obs_shape)


    def forward(self, inputs):
        raise NotImplementedError

    def update_limits(self, min, max):
        self.min = min
        self.max = max

    def act(self, inputs):
        output = self.adversary(inputs)
        action_means, action_std = torch.split(output, self.env_num)
        action_mean = action_means * (self.max - self.min) + self.min

        dist = MultivariateNormal(action_mean, torch.diag_embed(action_std*action_std))
        action = dist.sample()
        log_prob = dist.log_prob(action)
        action = torch.clamp(action, self.min, self.max)

        return action, log_prob

    def evaluate(self, input, action):
        # Sampling a distribution is not used when evaluating as it prevents the gradients from flowing.
        output = self.adversary(input)
        action_means, action_std = torch.split(output, self.env_num, 1)
        action_mean = action_means * (self.max - self.min) + self.min

        dist = MultivariateNormal(action_mean, torch.diag_embed(action_std*action_std+0.00001))
        entropy = dist.entropy()
        log_prob = dist.log_prob(action).unsqueeze(-1)

        return entropy.mean(), log_prob