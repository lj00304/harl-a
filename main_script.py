from algos.harl import *
from algos.hcp import *
from algos.harl_a import *
from algos.hcp_a import *
from algos.harl_sr import *
from algos.hcp_sr import *
from environments.gym_space_robot_v1_2.envs.space_robot_env import *
import argparse
import sys
import os

os.environ['KMP_WARNINGS'] = '0'

def main(args):
    architecture = args.E
    dim = args.dimension
    adversary = args.adversary

    if architecture == 'HARL':
        if args.environment != 'space_robot':
            if adversary:
                arch = harl_a(args)
            else:
                arch = harl(args)
            if dim == 1:
                arch.train_single()
            elif dim == 2:
                arch.train_double()
            else:
                sys.exit('dimension of design space unsuitable')
        elif args.environment == 'space_robot':
            env = SpaceRobotEnv(4)
            expert_network, obs = torch.load("./expert_networks/space_robot/run_1/space_robot.pt")
            model = PPO_modification(env, ref_frame='world', adversary=adversary)
            model.train(expert_network, log_interval=args.log_int, max_episodes=args.N, minimum=args.lower, maximum=args.upper)
        else:
            sys.exit('selected env does not exist')
    elif architecture == 'HCP':
        if args.environment != 'space_robot':
            if adversary:
                arch = hcp_a(args)
            else:
                arch = hcp(args)
            if dim == 1:
                arch.train_single()
            elif dim == 2:
                arch.train_double()
            else:
                sys.exit('dimension of design space unsuitable')
        elif args.environment == 'space_robot':
            env = SpaceRobotEnv(4)
            model = PPO_hardware(env, ref_frame='world', adversary=adversary)
            model.train(log_interval=args.log_int, max_episodes=args.N, minimum=args.lower, maximum=args.upper)
        else:
            sys.exit('selected env does not exist')
    else:
        sys.exit('selected architecture does not exist')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=('Implementation of harl or hcp with random selection and a parameter space dim'))
    parser.add_argument('-E', type=str, help='HCP or HARL', default='HCP')
    parser.add_argument('--adversary', type=bool, help='if the adversary network should be used or not', default=False)
    parser.add_argument('--dimension', type=int, help='number of design parameters to be update, 1 or 2', default=2)
    parser.add_argument('-N', type=int, help='number of environment steps', default=13e6)
    parser.add_argument('--log_int', type=int, help='Logging interval', default=50)
    parser.add_argument('--save_int', type=int, help='save interval', default=10)
    parser.add_argument('--upper', type=float, help='upper limit of morphology vector', default=0.1)
    parser.add_argument('--lower', type=float, help='lower limit of morphology vector', default=0.5)
    parser.add_argument('--environment', type=str, help='environment chosen for training, cart_pole, bipedal_walker or space_robot', default='space_robot')
    parser.add_argument('--test_iterations', type=int, help='K, number of updates used to calulate learning potential, only need if adversary is used', default=15)
    args = parser.parse_args()
    main(args)